<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- CSS GUE -->
      <style>
          <?php include_once ('css/style.css');?>
      </style>

      <!-- TITLE -->
    <title><?=$header;?></title>
  
  </head>
 
  <body>
        
  <!-- Fonts gue -->
  <link href="https://fonts.googleapis.com/css?family=Viga&display=swap" rel="stylesheet"> 

  <!-- NavBar gue -->
  <nav class="navbar navbar-expand-lg navbar-light">
  
    <div class="container">
    <a class="navbar-brand" href="#">MTXL</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav ml-auto">
        <a class="nav-item nav-link active" href="<?=site_url('buku');?>">Buku <span class="sr-only">(current)</span></a>
        <a class="nav-item nav-link" href="#">List Kajian</a>
        <a class="nav-item nav-link" href="<?=site_url('softland');?>">Softland</a>
        <a class="nav-item nav-link" href="<?=site_url('dashboard');?>">Dashboard</a>
        <a class="nav-item nav-link" href="<?=site_url('welcome');?>">Welcome</a>
        <a class="nav-item nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
        <a class="nav-item btn btn-primary tombol" href="#">join us</a>
      </div>
    </div>
  </div>
</nav>
  <!-- Akhir NavBar gue-->
  <!-- Jumbotron Gue -->
  <div class="jumbotron jumbotron-fluid" >
  <div class="container">
    <h1 class="display-4">Assalamu'alaikum</h1>
    <p class="lead">Website official Majelis Taklim XL Axiata</p>
  </div>
</div>
  <!-- Akhir Jumbotron -->






    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
