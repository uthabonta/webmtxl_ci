<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Buku_model');
	}

	public function index()
	{
		$query = $this->Buku_model->get(); //get disini adalah function di home_model
		$data['buku'] = $query->result();
		$data['header'] = 'Halaman Buku';

		$this->load->view('_header', $data);
		$this->load->view('buku_view');
		$this->load->view('_footer');
	}

	public function tambah ()
	{
		echo "halo";
		$data['header'] = 'Halaman Tambah Buku';
		$this->load->view('buku_tambah', $data);
	}

	public function ubah($id = null)
	{
		
		if ($id != null) {
			$data['header'] = 'Halaman Edit Buku '.$id;
			$query = $this->Buku_model->edit($id);
			$data['buku'] = $query->row();
			$this->load->view('buku_edit',$data);
        }
	}

	public function proses()
	{
		if (isset($_POST['add'])) {
			$inputan = $this->input->post(null, TRUE);
			$this->Buku_model->add($inputan);

			// $judul = $this->input->post('judul');
			// echo $judul;
		}else if (isset($_POST['edit'])) {
			$inputan = $this->input->post(null, TRUE);
			$this->Buku_model->update($inputan);

		}

		redirect('buku');
	}

	public function hapus($id = null)
	{
		if ($id != null) {
			$data['header'] = 'Halaman Hapus Data Buku '.$id;
			$this->Buku_model->delete($id);
		}
		
		redirect('buku');
	}
}
