<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data = array(
			'header' => 'Halaman Utama',
			'isi' => 'Selamat Datang di BELAJAR POS!'
		);
		$this->load->view('_header2',$data);
		$this->load->view('home_view');
		$this->load->view('_footer');
	}
}
