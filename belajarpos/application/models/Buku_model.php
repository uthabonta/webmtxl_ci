<?php
class Buku_model extends CI_Model {

    public function get($id = null) {

        $this->db->select('*');
        $this->db->from('tb_buku1');
        if ($id != null) {
            $this->db->where('id_buku', $id);
        }
        
        
        $query = $this->db->get();
        return $query;
    }

    public function add($data) {
        $param =array(
        'judul' /*ini dari nama field di database */ => $data['judul' /* ini dari name di form input */],
        'pengarang' => $data['pengarang'],
        'tahun_terbit'=>$data['tahun_terbit'],
        );

        $this->db->insert('tb_buku1', $param);
    }

    public function edit($id = null){
        $this->db->select('*');
        $this->db->from('tb_buku1');
        $this->db->where('id_buku',$id);
        if ($id != null) {
            $this->db->where('id_buku', $id);
        }
        
        
        $query = $this->db->get();
        return $query;
    }

    public function update($data)
    {
        $param=array(
            'judul'=>$data['judul'],
            'pengarang'=>$data['pengarang'],
            'tahun_terbit'=>$data['tahun_terbit'],
        );

        $this->db->where('id_buku', $data['id']);
        $this->db->update('tb_buku1', $param);
    }

    public function delete($id)
    {
        $param = $id;
        $this->db->where('id_buku', $param);
        $this->db->delete('tb_buku1');
    }
} 
?>